# mutt


```` 
netbsd@NomadBSD ~> pkg info | grep mutt
font-mutt-misc-1.0.3_4         X.Org miscellaneous Mutt fonts
mutt-2.1.5                     Small but powerful text based program for read/writing e-mail
neomutt-20210205               Bringing together all the Mutt Code
netbsd@NomadBSD ~> pkg info | grep ssl
brotli-1.0.9,1                 Generic-purpose lossless compression algorithm
flac-1.3.3                     Free lossless audio codec
gstreamer1-plugins-flac-1.16.2 GStreamer free lossless audio encoder/decoder plugin
jbigkit-2.1_1                  Lossless compression for bi-level images such as scanned pages, faxes
liblz4-1.9.3,1                 LZ4 compression library, lossless and very fast
wavpack-5.4.0                  Audio codec for lossless, lossy, and hybrid compression
netbsd@NomadBSD ~> pkg info | grep tls
gnutls-3.6.15                  GNU Transport Layer Security library
netbsd@NomadBSD ~> 
```` 

````
Mutt 2.1.5 (2021-12-30)
Copyright (C) 1996-2021 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: FreeBSD 13.0-RELEASE (amd64)
ncurses: ncurses 6.2.20200215 (compiled with 6.2)
libiconv: 1.16
libidn2: 2.3.0 (compiled with 2.3.2)
hcache backend: Berkeley DB 5.3.28: (September  9, 2013)

Compiler:
FreeBSD clang version 11.0.1 (git@github.com:llvm/llvm-project.git llvmorg-11.0.1-0-g43ff75f2c3fe)
Target: x86_64-unknown-freebsd13.0
Thread model: posix
InstalledDir: /usr/bin

Configure options: '--disable-fcntl' '--enable-compressed' '--enable-external-dotlock' '--enable-imap' '--enable-pop' '--enable-sidebar' '--sysconfdir=/usr/local/etc' '--with-docdir=/usr/local/share/doc/mutt' '--with-ssl=/usr' '--enable-autocrypt' '--with-sqlite3=/usr/local' '--enable-debug' '--disable-flock' '--enable-gpgme' '--without-gss' '--with-bdb=/usr/local' '--without-kyotocabinet' '--enable-hcache' '--without-tokyocabinet' '--with-libiconv-prefix=/usr/local' '--with-idn2=/usr/local' '--enable-locales-fix' '--enable-nls' '--with-sasl=/usr/local' '--enable-smtp' '--prefix=/usr/local' '--localstatedir=/var' '--mandir=/usr/local/man' '--disable-silent-rules' '--infodir=/usr/local/share/info/' '--build=amd64-portbld-freebsd13.0' 'build_alias=amd64-portbld-freebsd13.0' 'CC=cc' 'CFLAGS=-O2 -pipe  -I/usr/local/include/db5 -fstack-protector-strong -fno-strict-aliasing ' 'LDFLAGS= -L/usr/local/lib/db5  -fstack-protector-strong ' 'LIBS=' 'CPPFLAGS=' 'CPP=cpp'

Compilation CFLAGS: -Wall -pedantic -Wno-long-long -O2 -pipe  -I/usr/local/include/db5 -fstack-protector-strong -fno-strict-aliasing

Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  -USE_FCNTL  -USE_FLOCK   
+USE_POP  +USE_IMAP  +USE_SMTP  
+USE_SSL_OPENSSL  -USE_SSL_GNUTLS  +USE_SASL  -USE_GSS  +HAVE_GETADDRINFO  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  +HAVE_FUTIMENS  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  +CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  +LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_LIBIDN2  +HAVE_GETSID  +USE_HCACHE  
+USE_SIDEBAR  +USE_COMPRESSED  -USE_INOTIFY  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER

To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please contact the Mutt maintainers via gitlab:
    https://gitlab.com/muttmua/mutt/issues

patch-1.5.0.ats.date_conditional.1
patch-1.5.6.cb.reverse_reply.2
patch-1.5.7.ust.maildir-mtime.2
patch-1.5.4.cd.ifdef.1
vvv.quote
vvv.initials
````





